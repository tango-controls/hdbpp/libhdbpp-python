# version set in setup.cfg
RELEASE = (1,7,5)
version_major, version_minor, version_patch = RELEASE

from .reader import *
from .utils import *

